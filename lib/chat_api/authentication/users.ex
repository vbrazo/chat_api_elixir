defmodule ChatApi.Authentication.Users do
  @moduledoc """
  The boundry for the Users system
  """

  alias ChatApi.Repo
  alias ChatApi.Authentication.User

  def get_user!(id), do: Repo.get!(User, id)

  def update_user(user, attrs) do
    user
    |> User.changeset(attrs)
    |> ChatApi.Authentication.Auth.hash_password()
    |> Repo.update()
  end
end
