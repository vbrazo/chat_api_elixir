defmodule ChatApiWeb.GroupMessageController do
  use ChatApiWeb, :controller
  use ChatApiWeb.GuardedController

  alias ChatApi.{Chat, Repo}
  alias ChatApi.Chat.{GroupMessage}

  action_fallback(ChatApiWeb.FallbackController)

  plug(
    Guardian.Plug.EnsureAuthenticated
    when action in [:create, :show]
  )

  def create(conn, %{"group_message" => params}, user) do
    with {:ok, %GroupMessage{} = group_message} <- ChatApi.Chat.create_group_message(params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", group_message_path(conn, :show, group_message))
      |> render("show.json", group_message: group_message)
    end
  end

  def show(conn, %{"id" => id}, user) do
    group_message =
      id
      |> ChatApi.Chat.get_group_message()

    render(conn, "show.json", group_message: group_message)
  end
end
